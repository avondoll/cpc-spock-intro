package cpcpay;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Utility class used for user input parsing, allowing loosely-formatted time strings (8a, 1300, 9:00 PM, etc.) 
 * to be converted into a known "normalized" format (H:MM[AM|PM])
 */
public class TimeUtils {
    private static final Pattern timeInputPat = Pattern.compile("([01]?[0-9]|2[0123]):?([0-5][0-9])?\\s*(?i)(am|pm|a|p)?");
    private static final DateFormat dfTimeOut = new SimpleDateFormat("h:mma");

    // Given a loosely-formatted time string (ie 5p, 11:00, 700am), return a string
    // following the strict form: hh:mmAA (no leading zero on two-digit hours)
    public static String normalizeTime(String s) {
        if (s == null || "".equals(s?.trim())) { return s; }

        Calendar c = parseTimeLenient(s);
        String normTime = formatTime(c?.getTime(), s);

        //log.debug("normalizeTime('${s}') - returning '${normTime}'");
        return normTime;
    }

    public static String normalizeTime(String s, String refTime) {
        String sUc = s?.trim()?.toUpperCase() ?: "";
        // if no ref time is given, or our input string ends with:
        // "AM", "PM", "A", or "P" (case insensitive), don't do any special handling
        if (refTime == null || sUc.endsWith("A") || sUc.endsWith("P") || sUc.endsWith("AM") || sUc.endsWith("PM")) {
            return normalizeTime(s);
        } else {
            Calendar c = parseTimeLenient(s);
            Calendar cRef = parseTimeLenient(refTime);
            if (c != null && cRef != null) {
                // If the parsed time falls before the "reference" time (ie. an in punch),
                // add 12 hours.  This handles am/pm ambiguity, for example: 8-9, 1p-3, 11-12:30
                if (c.before(cRef)) {
                    c.add(Calendar.HOUR_OF_DAY, 12)
                // If more than 12 hours apart, subtract 12.  This handles cases like 9am-10, which 
                // would otherwise yield 9am-10pm
                } else if (c.get(Calendar.HOUR_OF_DAY) - cRef.get(Calendar.HOUR_OF_DAY) > 12) {
                    c.add(Calendar.HOUR_OF_DAY, -12)
                }
            }
            return formatTime(c?.getTime(), s);
        }
    }

    private static String formatTime(Date dt, String dflt) {
        String tm = "";
        if (dt != null) {
            tm = dfTimeOut.format(dt);
        } else {
            tm = dflt; // return value value if no date was provided
        }
        return tm;
    }

    final static Map<String,Calendar> TIME_CACHE;

    // populate a simple lookup table of free-form time strings to corresponding Calendar objects
    static {
        TIME_CACHE = Collections.synchronizedMap(new HashMap<String,Calendar>());
        for (int h = 1; h < 24; h++) {
            Calendar c = clearTime(Calendar.getInstance());
            int hAdj = (h <= 5) ? (h + 12) : h;  // hour val <= 5 implies PM
            String ap = hAdj < 12 ? "AM" : "PM"
            c.set(Calendar.HOUR_OF_DAY, hAdj);
            TIME_CACHE.put("${h}".toString(), c);
            TIME_CACHE.put("${h}${ap}".toString(), c);
            TIME_CACHE.put("${h} ${ap}".toString(), c);
            TIME_CACHE.put("${h}:00".toString(), c);
            for (int m = 15; m < 60; m = m + 15) {
                c = clearTime(Calendar.getInstance());
                c.set(Calendar.HOUR_OF_DAY, hAdj);
                c.set(Calendar.MINUTE, m);
                TIME_CACHE.put("${h}${m}".toString(), c);
                TIME_CACHE.put("${h}:${m}".toString(), c);
            }
        }
    }

    /**
     * Given a loosely-formatted time string (ie 5p, 11:00, 700am), return a Date object
     * with the time portion set to the parsed time (THE DATE PORTION OF RETURNED Calendar OBJ SHOULD BE IGNORED)
     */
    public static Calendar parseTimeLenient(String s) {
        String st = s?.trim()?.toString();

        // check for a cached lookup value
        Calendar c = TIME_CACHE.get(st?.toUpperCase());
        if (c != null) {
            //log.info("parseTimeLenient('${s}') - cache hit: ${formatTime(c.getTime(), '')}");
            return c.clone();  // don't allow modification of cached objs
        }

        c = clearTime(Calendar.getInstance());
        int hh24 = 0, min = 0
        
        String input = s?.replaceAll("\\.", "");  // remove all periods
        Matcher m = timeInputPat.matcher(input);
        if (!m.matches()) {
            //log.debug("parseTimeLenient('${s}') - no pattern match");
            return null;  // no match, return original
        }
        
        for (int i = 1; i <= m.groupCount(); i++) {
            String grp = m.group(i);
            String grpLc = grp?.toLowerCase()?.trim();
            
            if (i == 1) {
                try { hh24 = Integer.parseInt(grp) } catch (Exception e) { }
            } else if (["pm", "p"].contains(grpLc)) {
                if (hh24 < 12) { hh24 += 12 } // 12pm == noon
            } else if (grp == null || "".equals(grp) || ["am", "a"].contains(grpLc)) {
                if (hh24 == 12) { hh24 = 0 }  // 12am == midnight
            } else {
                try { min = Integer.parseInt(grp) } catch (Exception e) { }
            }
        }

        // out of range conditions
        if (hh24 < 0 || hh24 >= 24 || min < 0 || min >= 60) {
            //log.debug("parseTimeLenient('${s}') - parsed value(s) out of range: ${hh24} / ${min}");
            return null;
        }

        c.set(Calendar.HOUR_OF_DAY, hh24);
        c.set(Calendar.MINUTE, min);

        // cache this mapping 
        if (st != null && TIME_CACHE.size() < 1000) {
            //log.debug("parseTimeLenient('${s}') - adding to cache: '${st.toUpperCase()}' -> ${formatTime(c.getTime(), '')} [${st.getClass()}]");
            TIME_CACHE.put(st.toUpperCase().toString(), c);
        }
        
        return c;
    }


    // there is an equivalent groovy method on Calendar, this is here for Java portability
    private static Calendar clearTime(Calendar c) {
        c.clear(Calendar.HOUR_OF_DAY);
        c.clear(Calendar.HOUR);
        c.clear(Calendar.MINUTE);
        c.clear(Calendar.SECOND);
        c.clear(Calendar.MILLISECOND);
        return c;
    }
  
}