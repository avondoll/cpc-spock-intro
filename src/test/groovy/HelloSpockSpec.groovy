class HelloSpockSpec extends spock.lang.Specification {

    def "palindromes"() {
        expect:
        "madam im adam" == "mada mi madam".reverse()
        "a man a plan a canal panama" == "a man a plan a canal panama".reverse()  // amanap lanac a nalp a nam a
    }

    def "datatables: length of CPC-IT department members' names"() {
        expect:
        name.size() == length

        where:
        name        | length
        "Eumi"      | 4
        "David"     | 5
        "Darren"    | 6
        "Maya"      | 4
        "Patrick"   | 3
        "Andrew"    | 6
        "Alex"      | 4
        "Allen"     | 5
        "Han"       | 3
    }
}  