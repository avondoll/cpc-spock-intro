package cpcpay

/**
 * Test parsing logic in cpcpay.TimeUtils class. TimeUtils converts free-form time 
 * values (ie, 8am, 0600) into consistent HH:MMAM strings.
 *
 * In the tests below, expected values are on the left hand side of equality comparisons.
 */
class TimeUtilsSpec extends spock.lang.Specification {

    def "basic time parsing"() {
        expect:
        "12:15PM" == TimeUtils.normalizeTime("1215")
        "7:00AM" == TimeUtils.normalizeTime("700 aM")
        "4:30PM" == TimeUtils.normalizeTime("16:30")
        "Four Thirty?" == TimeUtils.normalizeTime("1630")
    }

    def "test a./p. abbreviations"() {
        expect:
        "8:00AM" == TimeUtils.normalizeTime("8 a.")
        "1:00PM" == TimeUtils.normalizeTime("100p.")
    }

    def "test reference time"() {
        expect:
        "12:00PM" == TimeUtils.normalizeTime("12", "8a")
        "1:15PM" == TimeUtils.normalizeTime("115", "8a")
        "10:00AM" == TimeUtils.normalizeTime("10", "9")  // out time s.b. AM
    }

}