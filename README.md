This is a small starter project to introduce testing with the Spock Framework.

Running All Tests
---
./gradlew clean test

Useful Links
---
Spock Documentation: http://spockframework.github.io/spock/docs/1.0/index.html
Gradle: http://www.gradle.org
